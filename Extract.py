import os
from UnityPy import AssetsManager
from UnityPy.helpers.ImportHelper import list_all_files

dst = 'extracted'
os.makedirs(dst, exist_ok=True)

def save_obj(obj, path, name=False):
    path = os.path.join(dst, *path.split('/'))
    
    if getattr(obj,'type',None) not in ['Texture2D','Sprite']:
        return
    
    data = obj.read()
    
    if name:
        path = os.path.join(path, data.name)
    
    os.makedirs(os.path.dirname(path), exist_ok=True)
    
    if obj.type == 'Sprite':
        if not path.endswith('.png'):
            path += '.png'
        data.image.save(path)
    elif obj.type == 'Texture2D':
        if not path.endswith('.png'):
            path += '.png'
        if not os.path.exists(path):
            try:
                print(data.assets_file.full_name)
                print(path)
                print(data.m_TextureFormat)
                data.image.save(path)
                print()
            except:
                pass 
    

for fp in list_all_files('assets'):
    #fp = os.path.join('assets',fp)
    a = AssetsManager()
    a.load_file(fp)
    for asset in a.assets.values():
        len_container = len(asset.container)
        if len_container == 0:
            print(asset.file_name)
            continue
        elif len(asset.container) == 1:
            path, obj = list(asset.container.items())[0]
            if getattr(obj,'type',None) in ['Sprite', 'Texture2D']:
                save_obj(obj, path)
            else:
                path = os.path.dirname(path)
                for obj in asset.objects.values():
                    save_obj(obj, path, True)
        else:
            for path, obj in asset.container.items():
                save_obj(obj, path)
            