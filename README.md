# Witch's Weapon Data-Mine

This git contains tools to download and extract assets of the game [Witch's Weapon](https://play.google.com/store/apps/details?id=com.dmm.games.witchsweapon.google).

The ownership of the assets belongs to [DMMGAMES](https://games.dmm.com/).

## Scripts

### Requirements

```cmd
pip install UnityPy
```

### EXTRACT

extract.py extracts all images from assets and saves them in extracted.
The assets were extracted from the obb of the xapk.
